/*
 * Project Euler Problem 5
 * 
 * 2520 is the smallest number that can be divided by each of the numbers from
 * 1 to 10 without any remainder.
 *
 * What is the smallest positive number that is evenly divisible by all of the
 * numbers from 1 to 20?
 *
 * Viktor Barsk
 *
 */

#include <stdio.h>

int devisable(int num, int max) {
	for(int i = max;i > 1;i--) {
		if(num%i != 0) {
			return 0;
		}
	}
	return 1;
}
int main(void) {
	int max = 20;
	int num = 0;
	do {
		num += 20;
	}while(!devisable(num,max));
	printf("%d",num);
}
