/* 
 * Project Euler problem 15
 *
 * Starningin the top left corner of a 2×2 grid, there are 6 routes (without
 * backtracking) to the bottom right corner.
 * 
 * How many routes are there through a 20×20 grid?
 *
 * Viktor Barsk
 *
 *  40 välj 20
 *  |40|
 *  |20| 
 */
#include <stdio.h>

/* Returnerar hur många vägar som går att ta på gridet för att komma till hörnet
 *
 * Pre: rows,cols bredd/höjd räknat i antalet vägar så ett 2x2 grid ska ha rows = 3 och cols = 3
 *
 * Post: Returnerar antalet väger hittils, lägg på ett om vi nått slutet.
 */
void route(int rows, int cols,int down,long *numways) {
	if(down) {
		if(rows ==  1) {
			return;
		} 
		if(cols == 1) {
			(*numways)++;
			return;
		}
		route(rows-1,cols,1,numways);
		route(rows-1,cols,0,numways);
	} else {
		if(cols == 1) {
			return;
		}
		if(rows == 1) {
			(*numways)++;
			return;
		}
		route(rows,cols-1,1,numways);
		route(rows,cols-1,0,numways);
	}
	return;
}
long bruteRoute(int rows, int cols) {
	long numways = 0;
	rows++;
	cols++;
	route(cols,rows,0,&numways);
	route(cols,rows,1,&numways);
	return numways;
}

long smartRoute(int searchedRows, int searchedCols) {
	if(searchedRows < searchedCols) {
		// Rows should be bigger than columns for the algorith to work.
		int temp = searchedRows;
		searchedRows = searchedCols;
		searchedCols = temp;
	}
	long arr[searchedCols];	
	arr[0] = 1;
	arr[1] = 3;
	arr[2] = 6;
	int row = 3;
	while(row < searchedRows) {
		for(int cols = 1; cols < row;cols++) {
			arr[cols] = arr[cols-1] + arr[cols];
		}
		arr[row] = arr[row-1]*2;
		row++;
	
	}
	for(int cols = 1; cols < searchedCols;cols++) {
		arr[cols] = arr[cols-1] + arr[cols];
	}
	if(searchedRows > searchedCols) {
		return arr[searchedCols-1] + arr[searchedCols];
	} else {
		return arr[row-1]*2;
	}


}
void testSmartRootWithBrute(void) {
	for(int i = 3;i < 7;i++) {
		for(int j = 3;j < 7;j++) {
			if(smartRoute(i,j) != bruteRoute(i,j)) {
				printf("Failed test case %dx%d",i,j);
			}
		}
	}
}
int main(int argc, char *argv[]) {
	if(argc < 2) {
		printf("USAGE: %s [rows] [columns]",argv[0]);
	}
	int rows,cols;
	sscanf(argv[1],"%d",&rows);
	if( argc == 3) {
		sscanf(argv[2],"%d",&cols);
	} else { 
		cols = rows;
	}
//	testSmartRootWithBrute();
	printf("Number of routes on a %dx%d grid: %ld\n",rows,cols,smartRoute(rows,cols));
}
