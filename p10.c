/*
 * Project Euler Problem 7
 * 
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 *
 * Find the sum of all the primes below two million.
 *
 * Viktor Barsk
 *
 */

#include <stdio.h>
#include "isqrt.c" 	// Integer square root by Halleck's method, with Legalize's speedup
			// Found here: http://home.utah.edu/~nahaj/factoring/isqrt.c.html
			// Mirror: http://pastebin.com/q1LWwjG7

long long int primeSum(int nthPrime) {
	nthPrime -= 1; // Not saving 2 in the prime array so we can free up some space.
	int primes[nthPrime]; //  The primes
	primes[0] = 3;
	int n = 1; // The number of primes in the array
	int i = 3;	// Current number to check
	long long int sum = 5; 
	do {
		int isPrime = 1;
		i += 2;
		// Can i be divided by the previous numbers up to the
		// square root of i?
		int iSquareRooted = isqrt(i);
		for(int j = 0; iSquareRooted >= primes[j]; j++) {
			if(i % primes[j] == 0) {
				isPrime = 0;
				break;
			}
		}
		if(isPrime) {
			// If so add it to the primes array
			sum += i;
			primes[n] = i;
			n++;
		}
	} while (i < nthPrime); 
	return sum;
}

int main(void) {
	int d = 2000000;
	printf("The sum of the primes below %d %lld\n",d,primeSum(d));
}

