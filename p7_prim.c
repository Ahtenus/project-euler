/*
 * Project Euler Problem 7
 * 
 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
 * that the 6th prime is 13.
 * 
 * What is the 10 001st prime number?
 *
 * Viktor Barsk
 *
 */

#include <stdio.h>
#include "isqrt.c" 	// Integer square root by Halleck's method, with Legalize's speedup
			// Found here: http://home.utah.edu/~nahaj/factoring/isqrt.c.html
			// Mirror: http://pastebin.com/q1LWwjG7

int findPrimeMemArr(int nthPrime) {
	nthPrime -= 1; // Not saving 2 in the prime array so we can free up some space.
	int primes[nthPrime]; //  The primes
	primes[0] = 3;
	int n = 1; // The number of primes in the array
	int i = 3;	// Current number to check
	do {
		int isPrime = 1;
		i += 2;
		// Can i be divided by the previous numbers up to the
		// square root of i?
		int iSquareRooted = isqrt(i);
		for(int j = 0; iSquareRooted >= primes[j]; j++) {
			if(i % primes[j] == 0) {
				isPrime = 0;
				break;
			}
		}
		if(isPrime) {
			// If so add it to the primes array
			primes[n] = i;
			n++;
		}
	} while (n < nthPrime); 
	return i;
}

int main(void) {
	printf("The 10001:st prime is %d\n",findPrimeMemArr(10001));
}

int findPrimeWithoutArr(void) {

	//printf("The 10001:st prime is %d (No array) \n",findPrimeWithoutArr());
	
	/*
	 * Not as fast as the one above
	 */

	int i = 1;	// Talet vi kollar.
	int n = 1; // Hur många primtal vi har hittat.
	do {
		int isPrime = 1;
		i += 2;
		// Kolla om i går att dela med de omjämna talen
		// upp till roten ur i.
		int iSqrooted = isqrt(i);
		for(int j = 3; iSqrooted >= j; j++) {
			if(i % j == 0) {
				isPrime = 0;
				continue;
			}
		}
		if(isPrime) {
			n++;
			//printf("%d\t",i);		
		}
	} while (n < 9999); // Fortsätt tills vi hittat det 1001 primatet.
	return i;
}

