#!/usr/bin/python
# -*- coding: utf-8 -*-

# Project Euler problem 22
# 
# Using p22_names.txt (right click and 'Save Link/Target As...'), a 46K text file
# containing over five-thousand first names, begin by sorting it into
# alphabetical order. Then working out the alphabetical value for each name,
# multiply this value by its alphabetical position in the list to obtain a name
# score.
# 
# For example, when the list is sorted into alphabetical order, COLIN, which is
# worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would
# obtain a score of 938 * 53 = 49714.
# 
# What is the total of all the name scores in the file?
# 
import string

def letterPoints(name):
	score = 0
	for letter in name:
		score += string.ascii_uppercase.index(letter) + 1
	return score 

#Reads from file
f = open('p22_names.txt', 'r')
nameArr = eval('[' + f.read() + ']')
f.close()

nameArr.sort()

total = 0
row = 1

for name in nameArr:
	total = total + letterPoints(name) * row
	row += 1
print total
