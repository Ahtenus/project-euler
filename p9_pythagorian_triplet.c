/*
 * Project Euler Problem 9
 * 
 * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
 * 
 * a^2 + b^2 = c^2
 *
 * For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
 * 
 * There exists exactly one Pythagorean triplet for which a + b + c = 1000.
 * Find the product abc.
 *
 * Viktor Barsk
 *
 */

#include <stdio.h>
#include <stdlib.h>

int evenRoot(int x) {
	/* 
	 * In: int x
	 * Out:Square root of x, if an even one exists else: 0
	 *     if negative input returns -1
	 */
	if(x == 1) {
		return 1;
	} else if(x < 0) {
		return -1;
	}
	int i = 2;
	while(i*i < x) {
		i++;
	}
	if(i*i == x) {
		return i;
	}
	return 0;
}

void testEvenRoot(void) {
	int testCases[7][2] = {
		{4,2},
		{8,0},
		{9,3},
		{0,0},
		{1,1},
		{2,0},
		{-5,-1}};
	int failed = 0;
	for(int i = 0; i < 7;i++) {
		if(evenRoot(testCases[i][0]) != testCases[i][1]) {
			printf("Failed test case %d\n",testCases[i][0]);
			failed++;
		}
	}
	if(failed == 0) {
		printf("Passed all tests\n");
	}
}

int pythTriplet(int *aA,int *bA,int *cA,const int sum) {
	int a = *aA;
	int b = *bA;
	int c = *cA;
	for(b = 1; 2*b < sum;b++) {
		for(a = 1;a < b; a++) {
			c = evenRoot(a*a + b*b);
			if(c > 0 && a + b + c == sum) {
				*aA = a; *bA = b; *cA = c;
				return 1;
			}
		}
	}
	return 0;
}

int main(void) {
	const int sum = 1000;	
	testEvenRoot();

	int a, b, c;
	pythTriplet(&a,&b,&c,sum);

	printf("a:%d b:%d c:%d\n",a,b,c);
	printf("Product:%d",a*b*c);
}
