/*
 * Project Euler Problem 4
 * 
 * A palindromic number reads the same both ways. The largest palindrome made
 * from the product of two 2-digit numbers is 9009 = 91 × 99.
 *
 * Find the largest palindrome made from the product of two 3-digit numbers.
 *
 * Viktor Barsk
 *
 */

#include <stdio.h>

int isPalindrome(int num) {
	char charNum[6];
	int n = sprintf(charNum,"%d",num);
	for(int i = 0;i < n;i++) {
		if(charNum[i] != charNum[n-1-i])
			return 0;
	}
	return 1;
}
int testPalindrome(void) {
	int testCases[7][2] = {
		{102,0},
		{45,0},
		{12321,1},
		{4,1},
		{10,0},
		{995599,1},
		{998001,0}};
	for(int i = 0; i < 7;i++) {
		if(isPalindrome(testCases[i][0]) != testCases[i][1]) {
			printf("isPalindrome failed test case %d\n",testCases[i][0]);
		} else {
			printf("isPalindrome passed test case %d\n",testCases[i][0]);
		}
	}
	return 1;
}


int main(void) {
	testPalindrome();	
	// Om palindrom spara värdet om talet är större än förra palindromet.

	/* Loopa igenom alla tal på detta vis där x är multiplikationerna som
	 * genomförs 
	 * 
	 * 1 x
	 * 2 x x
	 * 3 x x x
	 * 4 x x x x
	 * 5 x x x x x
	 * 6 x x x x x x
	 * 7 x x x x x x x
	 * 8 x x x x x x x x
	 * 9 x x x x x x x x x
	 *   1 2 3 4 5 6 7 8 9
	 */
	int biggestPalindrome = 0;
	int savedI,savedJ;
	for(int i = 1;i <= 999;i++) {
		for(int j = 1;j <= i;j++) {
			if(i*j > biggestPalindrome && isPalindrome(i*j)) {
				biggestPalindrome = i*j;
				savedI = i;
				savedJ = j;
			}
		//	printf("%d\t",i*j);
		}
	}
	printf("The biggest palindrome product made from two 3-digit numbers is %d * %d = %d",savedI,savedJ,biggestPalindrome);
}
