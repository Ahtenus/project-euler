/*
 * Project Euler Problem 17
 *
 * If the numbers 1 to 5 are written out in words: one, two, three, four, five,
 * then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
 * 
 * If all the numbers from 1 to 1000 (one thousand) inclusive were written out in
 * words, how many letters would be used?
 * 
 * NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and
 * forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20
 * letters. The use of "and" when writing out numbers is in compliance with
 * British usage.
 * 
 * Viktor Barsk
 *
 * NOTE: Could have been done on paper...
 */
/*
123456789012345678901234
fivehundredandtwelve
twohundredandseventyfour
twohundredandseventy
onethousand
fivehundredandthree
twelve
twenty
*/
#include <stdio.h>
void testNumLetters(void);
int numLetters(int num);
int main() {
	int letterSum = 0;
	for(int i; i <= 1000;i++) {
		letterSum += numLetters(i);
	}
	printf("Letters in the numbers from 1 to one thousand: %d\n", letterSum);
}
void testNumLetters(void) {
	int testCases[8][2] = {
		{3,5},
		{274,24},
		{270,20},
		{503,19},
		{12,6},
		{20,6},
		{1000,11},
		{512,20}
	};
	int failed = 0;
	for(int i = 0; i < 8;i++) {
		int returnedValue = numLetters(testCases[i][0]);
		if(returnedValue != testCases[i][1]) {
			printf("Failed test case %d\treturns: %d\tshould be: %d \n",testCases[i][0], returnedValue, testCases[i][1]);
			failed++;
		} else {
		//	printf("Passed test case %d\n",testCases[i][0]);
		}
	}
	if(failed == 0) {
		printf("Passed all tests!\n");
	}
}


int numLetters(int num) {
	int unit[] = {
		 0 // -
		,3 // One
		,3 // Two
		,5 // Three
		,4 // Four
		,4 // Five
		,3 // Six
		,5 // Seven
		,5 // Eight
		,4 // Nine
		,3 // Ten
		,6 // Eleven
		,6 // Twelve
		,8 // Thirteen
		,8 // Fourteen
		,7 // Fifteen
		,7 // Sixteen
		,9 // Seventeen
		,8 // Eighteen
		,8 // Nineteen
	};
	int ten[] = {
		 0
		,3 // Ten
		,6 // Twenty
		,6 // Thirty
		,5 // Forty
		,5 // Fifty
		,5 // Sixty
		,7 // Seventy
		,6 // Eighty
		,6 // Ninety
	};

	int hundred = 7;
	int thousand = 8;
	int and = 3;

	int letters = 0;
	if(num == 1000) {
		return unit[1]+thousand;
	}
	if(num/100 > 0) {
		letters += unit[num/100] + hundred;	
		// prinf("%d hundred ",unit[num/100]);
	}
	if(num/100 > 0 && num%100 > 0) {
		// prinf("and ");
		letters += and;
	}
	if(num%100 > 19) {
		letters += ten[(num%100)/10] + unit[num%10];
		// prinf("%d %d ", ten[(num%100)/10], unit[num%10]);
	} else {
		letters +=unit[num%100];
		// prinf("%d",unit[num%100]);
		
	}
	return letters;	
}
