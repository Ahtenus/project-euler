/*
 * Project Euler Problem 3
 *
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * 
 * What is the largest prime factor of the number 600851475143 ?
 *
 * Viktor Barsk
 *
 */

#include <stdio.h>

int main(void) {
	long long num = 600851475143LL;

	// Så länge alla faktorer inte har hittats
	// dela talet med i så många gånger som möjligt, där i = 2 och ökas.
	int i;
	for(i = 2;num > 1;i++) {
		while(num%i == 0) {
			num = num / i;
		printf("%lld\t%d\n",num,i);
		}
	}
	printf("%d",i-1);
}
