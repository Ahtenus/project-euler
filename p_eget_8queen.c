/*
 * Lösning på det klassiska 8 queen problemet
 * http://en.wikipedia.org/wiki/Eight_queens_puzzle
 *
 * Viktor Barsk 2012
 * WTFPL
 */
#include <stdio.h>
#include <stdbool.h>
#define BSIZE 8 

void printBoard(bool b[BSIZE][BSIZE]);
/*
 * okPos:	Returnerar true om pos är ok.
 *
 * In:		Bräde,rad och column att placera drottingen på
 */
bool okPos(bool b[BSIZE][BSIZE], int row, int col) {
	for(int i=0;i < BSIZE;i++) {
		// Rad & column
		if(b[row][i] && i != col)
			return false;
		if(b[i][col] && i != row) 
			return false;
	}
	for(int i=1;row+i < BSIZE && col+i < BSIZE;i++) {
		if(b[row+i][col+i])
			return false;
	}
	for(int i=1;row-i >= 0 && col-i >= 0;i++) {
		if(b[row-i][col-i])
			return false;
	}
	for(int i=1;row-i >= 0 && col+i < BSIZE;i++) {
		if(b[row-i][col+i])
			return false;
	}
	for(int i=1;row+i < BSIZE && col-i >= 0;i++) {
		if(b[row+i][col-i])
			return false;
	}
	return true;
}
void okPosTest(void) {
	bool b[BSIZE][BSIZE];
	for(int i = 0;i < BSIZE;i++) {
		for(int j = 0;j < BSIZE;j++) {
			b[i][j] = false;
		}
	}
	b[2][1] = true;
	b[1][6] = true;
	b[4][4] = true;


	int falseCases[6][2] = {
		{0,7},
		{0,0},
		{7,0},
		{7,7},
		{4,0},
		{7,4}
	};
	for(int i = 0; i < 6;i++) {
		if(okPos(b,falseCases[i][0],falseCases[i][1])) {
			printf("Failed test case %d %d\treturns: %d\n",falseCases[i][0],falseCases[i][1], true);
		}

	}
	int trueCases[4][2] = {
		{5,0},
		{0,2},
		{3,7},
		{7,5}
	};
	for(int i = 0; i < 4;i++) {
		if(!okPos(b,trueCases[i][0],trueCases[i][1])) {
			printf("Failed test case %d %d\treturns: %d\n",trueCases[i][0],trueCases[i][1], false);
		}

	}
	b[2][1] = false;
	b[1][6] = false;
	b[4][4] = false;
	b[0][0] = true;
	if(okPos(b,1,1)) {
		printf("Failed test case %d %d\t",1,1);
	}
}

/*
 * placeQueen:	Placerar ut en drottning på brädet
 *
 * In:		Bräde,rad att placera på
 * Post:	Ändat bräde 
 * Return: 	1 om placeringen gick bra 0 om det int finns någon fungerande plats.	
 */

bool placeQueen(bool b[BSIZE][BSIZE], int row) {
	if(row == BSIZE) 
		// Nått slutet sluta rekursionen
		return true;
	int tries = 1; // Vilken av de lediga platserna vi ska ta.
	do {
		int col = -1;
		int try = 0;
		while(try < tries) {
			col++;
			if(col == BSIZE) {
				// Det fanns ingen
				return false;
			}
			while(!okPos(b,row,col)) {
				// Hitta en ledig plats på denna rad.
				col++;
				if(col == BSIZE) {
					// Det fanns ingen
					return false;
				}
			}
			try++;
		//	printf("Try: %d row: %d\n col: %d\n",try, row, col);
		}
		b[row][col] = true;
		// Rekursera vidare, kom tillbaka om det inte fanns möjlighet
		// längre ner, isf ökar vi på så att vi ska hitta den
		// nästföljande lediga platsen.
		//printBoard(b);
		if(placeQueen(b,row+1)) {
			return true;
		} else {
			b[row][col] = false;
			tries++;
		}
	} while(true);
}

/*
 * printBoard:	printar ut spelplanen
 */
void printBoard(bool b[BSIZE][BSIZE]) {
	for(int i = 0;i < BSIZE;i++) {
		for(int j = 0;j < BSIZE;j++) {
			if(b[i][j])
				printf(" X");
			else
				printf(" .");
		}
		printf("\n");
	}

}
/*
 * clearBoard: sätter alla platser till false
 */
void clearBoard(bool b[BSIZE][BSIZE]) {
	for(int i = 0;i < BSIZE;i++) {
		for(int j = 0;j < BSIZE;j++) {
			b[i][j] = false;
		}
	}
}

int main(void) {
	okPosTest();
	bool b[BSIZE][BSIZE];
	clearBoard(b);
	placeQueen(b,0);
	printBoard(b);
}
